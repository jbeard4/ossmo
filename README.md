# OSSMO

![OSSMO Simple Diagram](https://gitlab.com/jbeard4/ossmo/raw/master/resources/ossmo.png "OSSMO Simple Diagram")

*ossmo* provides a new approach to sustainably fund open source projects.

The vision of ossmo is to enable zero-friction financial contributions from software developers who consume open source software (OSS), by allowing them to opt into mining crypto-currency on their desktop computer at the time the OSS is installed. The cryptocurrency mined is then split among OSS project authors on a ratio based on how much the OSS is used.

Here's a more complete diagram that illustrates how this all works:

![OSSMO Full Diagram](https://gitlab.com/jbeard4/ossmo/raw/master/resources/notes.png "OSSMO Full Diagram")

More specifically, OSSMO is a lerna repository of multiple node modules:

* [@ossmo/my-multi-miner](https://gitlab.com/jbeard4/ossmo/tree/master/packages/my-multi-miner): A package that scans the user's system, and installs and runs native mining binaries for their system.
* [@ossmo/client](https://gitlab.com/jbeard4/ossmo/tree/master/packages/client): A package that sits on top of @ossmo/my-multi-miner, and provides the user a series of prompts to request the user's consent to install and run mining binaries
* [@ossmo/my-ossmo-enabled-test-package](https://gitlab.com/jbeard4/ossmo/tree/master/packages/my-ossmo-enabled-test-package): A test package that illustrates how to "ossmo-ify" your OSS package, to enable it to mine for cryptocurrency.
* [@ossmo/prohashing-daemon](https://gitlab.com/jbeard4/ossmo/tree/master/packages/prohashing-daemon): One half of the server. This watches the prohashing.com mining pool API for changes, and inserts them into a postgresql database, so that statistics can be queried later (average hash power contributed by each OSS package).
* [@ossmo/bitcoind-daemon](https://gitlab.com/jbeard4/ossmo/tree/master/packages/bitcoind-daemon): The second half of the server. This watches a local bitcoin daemon for new transactions. It then computes how to split that transaction among multiple OSS authors based on the amount of hashing power contributed on behalf of each OSS package.

More information on each of these can be found in the README of each individual project.

# Demo

[![IMAGE ALT TEXT HERE](https://img.youtube.com/vi/gCZzi2i_vdI/0.jpg)](https://www.youtube.com/watch?v=gCZzi2i_vdI)

# FAQ

## I am an open source node.js module author. How do I enable my package to use ossmo?

Take a look at my [@ossmo/my-ossmo-enabled-test-package](https://gitlab.com/jbeard4/ossmo/tree/master/packages/my-ossmo-enabled-test-package). You basicaly need to do three things:

1. In package.json, add [@ossmo/client](https://gitlab.com/jbeard4/ossmo/tree/master/packages/client) as a dependency:

```json
  "dependencies": {
    "@ossmo/client": "^1.0.3"
  },

```

2. In package.json, create a field called "tips", and list your email address and BTC address:

```json
  "tips": {
    "jake@jacobeanrnd.com": {
      "btc": "3FjAWbhkXJGaPfz8mCqCufsBvQ4UgGdH5y"
    }
  },

```

3. Finally, in your module entry point (e.g. index.js), require and initiallize @ossmo/client:

```js
module.exports = async function myModuleEntryPoint(){
  await require('@ossmo/client')(require('./package.json').name);     //here's how you load ossmo
                                                                      //the prompts are asynchronous,
                                                                      //so you need to await

  console.log('Do some stuff here');
}
```

## Is this cryptojacking? 

My understanding is that "cryptojacking" is an industry term which refers to a situation where software attempts to mine cryptocurrency on a user's desktop without first seeking the user's consent (thus "hijacking" the user's system resources). The prevalence of cryptojacking has led to most commercial antivirus software identifying most modern mining software as malware. You can Google "how to prevent cryptojacking" to find a lot of articles on this.

This software does not enable cryptojacking, because when it is run, seeks the user's informed consent, so that the user opts in to mining. 

## Is this production-ready?

It's more of a proof-of-concept at this stage. But please feel free to try it out and report and problems you encounter.

## Will this support payouts in cryptocurrencies other than BTC?

I looked at Ethereum and Monero. The problem with Ethereum is that it seems to not support payments to multiple addresses in a single transaction. This feature is important for this project, as the initial incoming transaction amounts are likely to be small, so being able to split that into many smaller amounts and send it to many recipients in a single transaction is essential. Basically, splitting payment among multiple addresses in a single transaction is needed to enable microtransactions to OSS project authors.

Monero, on the other hand, is built to support private transactions, and thus makes it more difficult to see how funds flowed. OSSMO should be fully transparent.

Finally, it might one day be technically feasible to support BCH. 

# Support

To report a bug: [file an issue on GitLab](https://gitlab.com/jbeard4/ossmo/issues).

For general questions: [![Join the chat at https://gitter.im/ossmo-users/community](https://badges.gitter.im/ossmo-users/community.svg)](https://gitter.im/ossmo-users/community?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge)


