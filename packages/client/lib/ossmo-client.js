'use strict';

const { Confirm } = require('enquirer');
const {install, Miner} = require('@ossmo/my-multi-miner');
const fs = require('fs');
const path = require('path');
const childProcess = require('child_process');
const prompts = require('../resources/prompts');
const { npmPkgNameToProhashingGroupName } = require('./util');

module.exports = ossmoClient;

const pathToConfDir = path.join(process.env.HOME,'.ossmo');
const pathToConf = path.join(pathToConfDir,'conf.json');
const util = require('util');
const writeFile = util.promisify(fs.writeFile);

let config = {};

//TODO: check whether the user's system is one which is supported by the miner

async function ossmoClient(packageName) {
  let configIsInitialized = false;

  //check for configuration file and try to load existing config
  if(fs.existsSync(pathToConf)){
    try {
      config = require(pathToConf)
    } catch(e){
      config = {}; 
    }
  }

  if(!fs.existsSync(pathToConfDir)){
    fs.mkdirSync(pathToConfDir)
  } 

  //do the prompts
  await prompt1(packageName);
}

async function prompt1(packageName){
  if(typeof config.wouldYouLikeToHelpSupportThisProjectByMiningForCryptocurrency === 'undefined'){
    await doPrompt1(packageName);
  }else{
    if(config.wouldYouLikeToHelpSupportThisProjectByMiningForCryptocurrency){
      await prompt2(packageName);
    } else if(config.canWePromptYouAboutThisAgainInTheFuture){
      delete config.canWePromptYouAboutThisAgainInTheFuture;    //reset the prompt flag
      await doPrompt1(packageName);
    } else{
      //do nothing we are done
    }
  }
}

async function doPrompt1(packageName){
  config.wouldYouLikeToHelpSupportThisProjectByMiningForCryptocurrency = 
    await (new Confirm({message: prompts.wouldYouLikeToHelpSupportThisProjectByMiningForCryptocurrency(packageName), initial: true})).run();
  console.log('\n');
  await saveConfig();

  if(config.wouldYouLikeToHelpSupportThisProjectByMiningForCryptocurrency){
    await prompt2(packageName);
  }else {
    await handleWhenUserSaysNo();
  }
}

async function prompt2(packageName){
  if(typeof config.isItOKToDownloadThisSoftwareNow === 'undefined'){
    await doPrompt2(packageName);
  }else{
    if(config.isItOKToDownloadThisSoftwareNow ){
      const success = await installTheMiner();
      //TODO: check if installation succeeds
      await prompt3(packageName);
    } else if(config.canWePromptYouAboutThisAgainInTheFuture){
      delete config.canWePromptYouAboutThisAgainInTheFuture;    //reset the prompt flag
      await doPrompt2(packageName);
    } else{
      await handleWhenUserSaysNo();
    }
  }
}

async function doPrompt2(packageName){
  config.isItOKToDownloadThisSoftwareNow = await (new Confirm({message: prompts.isItOKToDownloadThisSoftwareNow, initial: true})).run();
  console.log('\n');
  await saveConfig();
  if(config.isItOKToDownloadThisSoftwareNow){
    const success = await installTheMiner();
    //TODO: check if installation succeeds
    await prompt3(packageName);
  }else{
    await handleWhenUserSaysNo();
  } 
}

async function prompt3(packageName){
  if(typeof config.wouldYouLikeToMineAnonymously === 'undefined'){
    config.wouldYouLikeToMineAnonymously = await (new Confirm({message: prompts.wouldYouLikeToMineAnonymously})).run();
    console.log('\n');
    await saveConfig();

    console.log(prompts.thanks);
  }

  const npmUsername = getNpmUsername(config.wouldYouLikeToMineAnonymously);
  startTheMiner(packageName, npmUsername);
}

async function handleWhenUserSaysNo(){
  if(typeof config.canWePromptYouAboutThisAgainInTheFuture === 'undefined'){
    config.canWePromptYouAboutThisAgainInTheFuture = await (new Confirm({message: prompts.canWePromptYouAboutThisAgainInTheFuture, initial: true})).run();
    console.log('\n');
    await saveConfig();

    if(!config.canWePromptYouAboutThisAgainInTheFuture){
      console.log(prompts.okNoPrompt);
    }
  } else {
    // done. silentely do nothing
    return null;
  }
}

async function installTheMiner(){
  //TODO: check if he is already installed 
  //TODO: hash the file to verify his integrity
  //TODO: check that the executable bit is set
  //install in ~/.ossmo
  await install(pathToConfDir);
}

function getNpmUsername(mineAnonymously){
  //TODO: make this an async function
  let npmUsername = null;
  if(!mineAnonymously){
    const {stdout, status, stderr} = childProcess.spawnSync('npm',['whoami']);
    if(status === 0){
      npmUsername = stdout.toString().slice(0, -1);
    }
  }
  return npmUsername; 
}

function startTheMiner(packageName, npmUsername){
  //start the miner
  const miner = new Miner(pathToConfDir);
  const safePackageName = npmPkgNameToProhashingGroupName(packageName);   //TODO: might be better to move this into my-multi-miner

  miner.launch({
    pkgName: safePackageName, 
    npmUsername,
    minerUrl: 'stratum+tcp://prohashing.com:3341',
    minerUsername: 'jbeard4',
    cwd: pathToConfDir,
    niceness: 19
  });
}

function saveConfig(){
  //write config to a file
  return writeFile(pathToConf, JSON.stringify(config, 4,4)) ;
}
