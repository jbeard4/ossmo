# @ossmo/client

![Screen Recording](https://gitlab.com/jbeard4/ossmo/raw/master/packages/client/screen-recording.gif "Screen Recording")

This package prompts the user, installs mining software, and begins mining.

## API


```js
function promptUser(packageName): Promise
```

## Example

```js
module.exports = async function myModuleEntryPoint(){
  await require('@ossmo/client')(require('./package.json').name);     //here's how you load ossmo
                                                                      //the prompts are asynchronous,
                                                                      //so you need to await

  console.log('Do some stuff here');
}
```

