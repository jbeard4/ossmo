module.exports = {
  wouldYouLikeToHelpSupportThisProjectByMiningForCryptocurrency : 
    (packageName) => (`
      Hi, it looks like this is the first time you are running ${packageName}.
      ${packageName} is supported by the community of its users.

      You can help sustain this project by mining for cryptocurrency on your
      computer. The mining process runs in the background with low priority so as to not
      disrupt other proceses, and 100% of the cryptocurrency mined goes toward the
      project authors (minus network fees). 

      Would you like to help support this project by mining for cryptocurrency?`),

  isItOKToDownloadThisSoftwareNow : 
    `Great! We will now download the mining binaries onto your system. Note that
    some antivirus software may flag these binaries as malware, but it is *not*
    malware. 

    Is it OK to download this software now?`,

  wouldYouLikeToMineAnonymously :
    `OK, one last thing. You can choose to mine anonymously, or you can 
    report your npm username to the mining server, so that you and other people can
    see how much hashing power you have contributed. 

    Would you like to mine anonymously?`,

  thanks: 
    `Thanks! You are now set up. You can change your configuration at any time by
    editing ~/.ossmo/config.json.  Logfiles will be stored in ~/.ossmo/logs

    Happy hashing!`,

  canWePromptYouAboutThisAgainInTheFuture : 
    'OK! Can we prompt you about this again in the future?',

  okNoPrompt : 'OK, we won\'t prompt you again.',
};
