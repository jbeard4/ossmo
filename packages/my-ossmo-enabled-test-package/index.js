module.exports = async function myModuleEntryPoint(){
  await require('@ossmo/client')(require('./package.json').name);     //here's how you load ossmo
                                                                      //the prompts are asynchronous,
                                                                      //so you need to await

  console.log('Do some stuff here');
}

if(require.main === module){
  module.exports();
}
