DROP TABLE miner_update;
DROP TABLE miner_connection;

CREATE TABLE miner_connection (
  first_username text,
  uuid uuid PRIMARY KEY,
  group_name text,
  miner_name text,
  algorithm_name text,
  coin_name text
);

CREATE TABLE miner_update (
  id SERIAL,
  miner_connection_uuid uuid REFERENCES miner_connection (uuid),
  usercount int,
  coin_is_static bool,
  adjusted_hashrate float,
  hashrate float,
  restart_time float,
  actual_difficulty int,
  difficulty int,
  difficulty_is_static bool,
  static_coin json,
  work_restart_proportion float,
  min_restart_delay float,
  min_network_difficulty int,
  work_restart_penalty int,
  share_count int,
  first_reg_share_time float,
  errors json,
  server_time float,
  is_solo bool,
  auxiliary_mode bool,
  work_refresh_mode bool
);
