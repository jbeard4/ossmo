const autobahn = require('autobahn');
const key = process.env.PROHASHING_API_KEY;
const {
  upsertIntoMinerConnection,
  insertIntoMinerUpdate,
  start
} = require('./pg');
const {
  asyncForEach
} = require('./util');

var client;

var wampConnection = null;
var wampSession = null;
var wampUser = 'web';
var wampPassword = 'web';

//If using node.js, the following code resolves an issue where the Electronic Frontier Foundation's free certificates are not trusted.
//process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

function onChallenge(_wampSession, method, extra) {
  wampSession = _wampSession;
  if (method == 'wampcra') {
    return autobahn.auth_cra.sign(wampPassword, extra.challenge);
  }
};

function connectionOpen(session, details) {
  wampSession.subscribe('found_block_updates', onBlockUpdate);
  wampSession.call('f_all_miner_updates', [key]).then(initialSessionUpdatesReceived);
};

function onBlockUpdate(block) {
  //Handle found blocks here.
};

async function initialSessionUpdatesReceived(updates) {
  //Handle the initial miner information here.
  updates.forEach( update => delete update.api_key );
  console.log('initialSessionUpdatesReceived updates', updates);
  asyncForEach(updates, async function(update){
    await upsertIntoMinerConnection(client, update);
    await insertIntoMinerUpdate(client, update);
  })

  //After handling the initial information, now subscribe to receive future updates.
  wampSession.subscribe(`miner_update_diffs_${key}`, onMinerUpdate);
};

async function onMinerUpdate(updates) {
  updates.forEach( update => delete update.api_key );
  //Handle live miner updates here.
  console.log('updates', updates);
  asyncForEach(updates, async function(update){
    await upsertIntoMinerConnection(client, update);
    await insertIntoMinerUpdate(client, update);
  })
};

start(async function(c){
  client = c;
  wampConnection = new autobahn.Connection({
    url : 'wss://live.prohashing.com:443/ws',
    realm : 'mining',
    authmethods: ['wampcra'],
    authid: wampUser,
    onchallenge: onChallenge,
    tlsConfiguration : {}
  });
  wampConnection.onopen = connectionOpen;
  wampConnection.open();
});
