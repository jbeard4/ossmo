const { Client } = require('pg')

async function upsertIntoMinerConnection(client, o){
  console.log('upsertIntoMinerConnection', o);

  const props = [
    'first_username',
    'uuid',
    'group_name',
    'miner_nam',
    'algorithm_name',
    'coin_name'
  ];

  const notNullProps = props.filter( prop => o[prop] )

  const text = `INSERT INTO miner_connection(first_username, uuid, group_name, miner_name, algorithm_name, coin_name) VALUES($1, $2, $3, $4, $5, $6) 
    ON CONFLICT (uuid) 
    DO 
      UPDATE 
        SET 
          ${notNullProps.map( (prop, i) => `${prop} = EXCLUDED.${prop}${i === (notNullProps.length - 1) ? '' : ','}` ).join('\n')}
        RETURNING *`
    

  const values = props.map( prop => o[prop] ) 

  //console.log('text',text);
  //console.log('values',values);

  return doQuery(client, text, values, upsertIntoMinerConnection.name);
}


async function insertIntoMinerUpdate(client, o){
  console.log('insertIntoMinerUpdate', o);

  const text = 'INSERT INTO miner_update(miner_connection_uuid, usercount, coin_is_static, adjusted_hashrate, hashrate, restart_time, actual_difficulty, difficulty, difficulty_is_static, static_coin, work_restart_proportion, min_restart_delay, min_network_difficulty, work_restart_penalty, share_count, first_reg_share_time, errors, server_time, is_solo, auxiliary_mode, work_refresh_mode) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21) RETURNING *'

  const values = [ o.uuid, o.usercount, o.coin_is_static, o.adjusted_hashrate, o.hashrate, o.restart_time, o.actual_difficulty, o.difficulty, o.difficulty_is_static, o.static_coin, o.work_restart_proportion, o.min_restart_delay, o.min_network_difficulty, o.work_restart_penalty, o.share_count, o.first_reg_share_time, o.errors, o.server_time, o.is_solo, o.auxiliary_mode, o.work_refresh_mode ]

  return doQuery(client, text, values, insertIntoMinerUpdate.name);
}

async function doQuery(client, text, values, name){
  // async/await
  try {
    const res = await client.query(text, values)
    //console.log(name, 'success', res.rows[0])
    return res;
  } catch(err) {
    console.log(name, 'error', err.stack)
  }

}

async function start(cb){
  const client = new Client()
  await client.connect()
  await cb(client);
}

//upsertIntoMinerConnection(initialSessionUpdate);
//insertIntoMinerUpdate(minerUpdate);

//average by group/pkg name
async function getAverageHashratesPerPackage(client, to, from){
  const text = `select group_name, avg(adjusted_hashrate) from miner_update inner join miner_connection on miner_update.miner_connection_uuid = miner_connection.uuid where adjusted_hashrate is not null and server_time < ${to} ${from ? `and server_time > ${from}` : '' } group by group_name`;

  return doQuery(client, text, [], getAverageHashratesPerPackage.name);
}

//other queries that might be useful:
'select * from miner_update inner join miner_connection on miner_update.miner_connection_uuid = miner_connection.uuid'

//get all adjusted hashrates between two dates
'select group_name, adjusted_hashrate, server_time from miner_update inner join miner_connection on miner_update.miner_connection_uuid = miner_connection.uuid where adjusted_hashrate is not null and server_time > 1547769864 and server_time < 1547856264  order by server_time'

//average by miner name
'select miner_name, avg(adjusted_hashrate) from miner_update inner join miner_connection on miner_update.miner_connection_uuid = miner_connection.uuid where adjusted_hashrate is not null and server_time > 1547769864 and server_time < 1547856264  group by miner_name'

module.exports = {
  insertIntoMinerUpdate,
  upsertIntoMinerConnection,
  getAverageHashratesPerPackage,
  start,
  doQuery
}
