# @ossmo/prohashing-daemon

This watches the prohashing API for updates, and inserts the updates into a
Postgresql database.

## Environment variables

The following variables must be set in your environment.

* PROHASHING_API_KEY: Proshashing API key
* PGHOST: Postgresql host
* PGPASSWORD: Postgresql password
* PGUSER: Postgresql username
* PGDATABASE: Postgresql database name
* PGPORT: Postgresql port
