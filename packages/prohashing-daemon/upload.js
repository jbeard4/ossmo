const { rows } = require('./out.json');
const {
  upsertIntoMinerConnection,
  insertIntoMinerUpdate,
  start
} = require('./pg');
const {
  asyncForEach
} = require('./util');

start(async function(client){
  return asyncForEach(rows, async function(row){
    await asyncForEach(row.doc.updates, async function(update){
      if(
        update.group_name && 
        update.coin_name &&
        update.algorithm_name 
      ){
        await upsertIntoMinerConnection(client, update);
      }
      await insertIntoMinerUpdate(client, update);
    }); 
  })
});
