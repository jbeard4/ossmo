CREATE TABLE transactions (
  address char(34),
  category text,
  amount float,
  label text,
  vout int,
  confirmations int,
  blockhash text,
  blockindex int,
  blocktime int,
  txid char(64) primary key,
  walletconflicts json,
  time int,
  timereceived int
);


CREATE TABLE transaction_disbursements (
  txid char(64) REFERENCES transactions (txid),
  disbursement_txid char(64) primary key
);
