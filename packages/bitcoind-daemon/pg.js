const { Client } = require('pg')
const {
  doQuery
} = require('prohashing-daemon/pg');

async function upsertIntoTransactions(client, o){
  //console.log('upsertIntoTransactions', o);
  const text = `INSERT INTO transactions(address, category, amount, label, vout, confirmations, blockhash, blockindex, blocktime, txid, walletconflicts, time, timereceived) VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13) 
    ON CONFLICT (txid) 
    DO 
      UPDATE 
        SET 
          address = EXCLUDED.address,
          category = EXCLUDED.category,
          amount = EXCLUDED.amount,
          label = EXCLUDED.label,
          vout = EXCLUDED.vout,
          confirmations = EXCLUDED.confirmations,
          blockhash = EXCLUDED.blockhash,
          blockindex = EXCLUDED.blockindex,
          blocktime = EXCLUDED.blocktime,
          walletconflicts = EXCLUDED.walletconflicts,
          time = EXCLUDED.time,
          timereceived = EXCLUDED.timereceived
        RETURNING *`
    

  const values = [ 
    o.address,
    o.category,
    o.amount,
    o.label,
    o.vout,
    o.confirmations,
    o.blockhash,
    o.blockindex,
    o.blocktime,
    o.txid,
    o.walletconflicts,
    o.time,
    o.timereceived
  ]

  return doQuery(client, text, values, upsertIntoTransactions.name);
}

async function insertIntoTransactionDisbursements(client, o){
  const text = 'insert into transaction_disbursements(txid, disbursement_txid) values ($1,$2)';
  const values = [ 
    o.txid,
    o.disbursement_txid
  ]

  return doQuery(client, text, values, insertIntoTransactionDisbursements.name);
}

const t = (not) => `select t.*, d.disbursement_txid from transactions t left outer join transaction_disbursements d on t.txid = d.txid where d.disbursement_txid is ${not ? 'not' : ''} null and t.category = 'receive'`    //TODO: add confirmations back in: 'and t.confirmations > 6'

async function getIncomingUndisbursedTransactions(client){
  const text = t(false)
  return doQuery(client, text, [], getIncomingUndisbursedTransactions.name);
}

async function getPreviousIncomingDisbursedTransaction(client, time){
  const text = `${t(true)} and t.time < ${time} limit 1`

  return doQuery(client, text, [], getPreviousIncomingDisbursedTransaction.name);
}

module.exports = {
  upsertIntoTransactions,
  getIncomingUndisbursedTransactions,
  getPreviousIncomingDisbursedTransaction,
  insertIntoTransactionDisbursements
}
