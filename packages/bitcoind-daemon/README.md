# @ossmo/bitcoind-daemon

This watches a local bitcoin daemon for incoming transactions, then queries the
local postgresql database for statistics on avarage hashing performance for
each npm package since last incoming transaction (populated from the
prohashing.com mining server API), then queries the npm server for recursive
packages dependencies and npm package authors that list their bitcoin wallet
address, then computes the disbursement, creates a new transaction and sends it
to the bitcoin network.

## Environment variables

The following variables must be set in your environment.

* BITCOIND_RPCCONNECT: bitcoind host
* BITCOIND_RPCUSER: bitcoind user
* BITCOIND_RPCPASSWORD: bitcoind password
* BITCOIND_WALLET_NAME: bitcoin wallet name
* BITCOIND_WALLET_PASSPHRASE: bitcoin wallet password
* PGHOST: Postgresql host
* PGPASSWORD: Postgresql password
* PGUSER: Postgresql username
* PGDATABASE: Postgresql database name
* PGPORT: Postgresql port

