const path = require('path');
const fs = require('fs');
const childProcess = require('child_process');
const util = require('util');
const exec = util.promisify(childProcess.exec); 
const mkdir = util.promisify(fs.mkdir);
const exists = util.promisify(fs.exists);
const {
  upsertIntoTransactions,
  getIncomingUndisbursedTransactions,
  getPreviousIncomingDisbursedTransaction,
  insertIntoTransactionDisbursements
} = require('./pg');
const {
  prohashingGroupNameToNpmPkgName
} = require('@ossmo/client/lib/util');

const {
  getAverageHashratesPerPackage,
  start
} = require('@ossmo/prohashing-daemon/pg');

const cmd = 'bitcoin-cli' 
const defaultArgs = [
  `--rpcconnect=${process.env.BITCOIND_RPCCONNECT}`,
  `--rpcuser=${process.env.BITCOIND_RPCUSER}`,
  `--rpcpassword=${process.env.BITCOIND_RPCPASSWORD}`
]
const walletName = process.env.BITCOIND_WALLET_NAME;
const walletPassphrase = process.env.BITCOIND_WALLET_PASSPHRASE;
const walletArg = `--rpcwallet=${walletName}`;

const TMPDIR = '/tmp';
const TMPDIR_NODEMODULES = path.join(TMPDIR,'node_modules');
let client;

async function doCommand(args, skipParseAsJson){
  const command = [cmd].concat(args).join(' ');
  //console.log('doCommand', command);
  let {stdout, stderr, error} = await exec(command);
  //console.log(stdout, stderr, error)
  if(!error){
    const o = skipParseAsJson ? stdout : JSON.parse(stdout);
    //console.log(o);
    return o;
  } else {
    return error;
  }
}

//this is where the magic happens
async function poll(){
  const arr = await doCommand(defaultArgs.concat([walletArg, 'listtransactions', 'ossmo2']));
  //ideally, we look to see what new transactions have gone from < 6 confirmations to >= 6 confirmations
  for(let o of arr){
    await upsertIntoTransactions(client, o);
  }
  const transactionsToDisburse = await getIncomingUndisbursedTransactions(client);

  //for each disbursed transaction
  for(transaction of transactionsToDisburse.rows){
    console.log('transaction ', transaction) 

    const { amount, txid } = transaction;  //the amount to be disbursed

    //get the previous disbursed transaction
    const prevDisbursedTransaction = await getPreviousIncomingDisbursedTransaction(client, transaction.time);
    console.log('prevDisbursedTransaction ', prevDisbursedTransaction.rows )

    //use 'time' field to set the interval
    const prevDisbursedTransactionTime = prevDisbursedTransaction.rowCount ? 
      prevDisbursedTransaction.rows[0].time : 
      null;

    //query the average hashrates for each package
    const averageHashratesPerPackage = await getAverageHashratesPerPackage(client, transaction.time, prevDisbursedTransactionTime);
    console.log('averageHashratesPerPackage ', averageHashratesPerPackage );

    //query the package server for author payment info
    //for each package name
    const allStats = {}
    const combinedHashrateOfAllPackages = averageHashratesPerPackage.rows.map( row => row.avg).reduce( (a,b) => a + b, 0)
    for(let {group_name, avg} of averageHashratesPerPackage.rows){
      //remember to convert the group_name back to the package format using prohashing-client/util
      const pkgName = prohashingGroupNameToNpmPkgName(group_name)
      console.log('installing pkgName ', pkgName );
      //npm install it
      //TODO: it would actually be better to use spawn, and to stream this output, as it can take some time to complete
      const installDir = path.join(TMPDIR, pkgName);
      const installDirNodeModules = path.join(installDir, 'node_modules') 

      //initialize all directories
      const arr = pkgName.split('/')
      if(arr.length > 1){
        const p = path.join(TMPDIR,arr[0]);
        const exists1 = await exists(p)
        if(!exists1){
          await mkdir(p)
        }
      }

      const exists2 = await exists(installDir)
      if(!exists2){
        await mkdir(installDir)
      }

      const exists3 = await exists(installDirNodeModules)
      if(!exists3){
        await mkdir(installDirNodeModules)
      }
      let {stdout, stderr, error} = await exec(`npm install ${pkgName}`, {cwd : installDir});  //TODO: would be better to run these in parallel
   
      if(!error){
        //traverse the node_modules to retrieve all package.json modules
        let {stdout, stderr, error} = await exec(`find ${installDir} -name "package.json"`)

        if(!error){
          //extract authors field
          const packagePaths = stdout.toString().split('\n').filter( pkgPath => pkgPath);
          const packages = packagePaths.map( pkgPath => require(pkgPath)); //FIXME: 'require' here is actually synchronous. would be better to be async
          
          //gather "btc" fields 
          //  originally, I wanted to put an extra 'btc' field on the author object
          //  but npm filters out extra fields on the author object 
          /*
          const allAuthors = 
            packages.
              map( pkg => (
                pkg.author ? [pkg.author] : ( 
                  pkg.authors ? pkg.authors : [] 
                ))).
              reduce((a,b) => a.concat(b), []);
          console.log('all authors for package', pkgName, allAuthors);
          const authorsWithPayableAddresses = allAuthors.filter( author => author.btc );
          */
          //TODO: initialize addresses to hold funds for authors who do not yet have btc information
          //  -> later, integrate with npmjs to authenticate them, and release funds to them on successful authentication
          const authorsWithPayableAddresses = {}
          packages.forEach( pkg => {
            if(pkg.tips){
              Object.keys(pkg.tips).forEach( authorEmail => {
                authorsWithPayableAddresses[authorEmail] = pkg.tips[authorEmail].btc;
              })
            }
          });
          console.log('authors with payable addresses', pkgName, authorsWithPayableAddresses);

          //compute the disbursement (amount, average package stats, # of author for each package)
          // here's the algorithm: 
          //    first split transaction amount based on avg hashrate between the packages; 
          //    then for each of those splits, split evenly again between each package author
          const percentOfCombinedHashrate = avg / combinedHashrateOfAllPackages;
          const pkgShare = percentOfCombinedHashrate * amount;
          const authorShare = pkgShare / Object.keys(authorsWithPayableAddresses).length;

          allStats[pkgName] = {
            authorsWithPayableAddresses,
            averageHashrate: avg,
            percentOfCombinedHashrate,
            pkgShare,
            authorShare 
          }
        }else{
          console.error('find command failed for package', pkgName);
          console.error(error);
        }

      } else {
        console.error('npm install command failed for package', pkgName);
        console.error(error);
      }
    } 

    console.log('pkg stats (avg hashrate, pkg authors) for this round', allStats)

    //flatten him out to the appropriate data structure
    const addressAmountMap = {},
          feeSplitArray = []
    Object.keys(allStats).forEach( pkgName => {
      const stats = allStats[pkgName]
      Object.keys(stats.authorsWithPayableAddresses).forEach( authorEmail => {
        const btcAddress = stats.authorsWithPayableAddresses[authorEmail];
        addressAmountMap[btcAddress] = stats.authorShare;
        feeSplitArray.push(btcAddress) 
      })
    })


    console.log('unlocking the wallet');
    await doCommand(defaultArgs.concat([walletArg, 'walletpassphrase', '"' + walletPassphrase + '"', 60]), true);

    //TODO: I can optimize this by combining all input transactions into a single raw transaction
    
    console.log('preparing raw transaction',addressAmountMap);
    const hex = await doRawTransaction(transaction, addressAmountMap);
    console.log('initial raw transaction hex', hex);

    console.log('funding raw transaction');
    const {fee} = await doCommand(defaultArgs.concat([walletArg, 'fundrawtransaction', hex]));
    console.log('funding response', fee);

    //now we know what the fee estimate is, go back and adjust what we were planning to pay
    //so that the fee is subtracted and we don't get an 'insufficient funds' error
    const percentageChange = (amount - fee) / amount;
    const addressAmountMap2 = {}
    Object.keys(addressAmountMap).forEach( address => addressAmountMap2[address] = percentageChange * addressAmountMap[address] )

    console.log('original amount was', amount, 'minus fee', fee, 'new amount is', amount - fee);
    console.log('adjusting disbursement by %', 100 - percentageChange * 100);
    console.log('preparing to now send ', addressAmountMap2);
    const hex2 = await doRawTransaction(transaction, addressAmountMap2);
    console.log('raw transaction 2 hex is ', hex2);

    console.log('signing hex2');
    const {hex : signedHex} = await doCommand(defaultArgs.concat([walletArg, 'signrawtransactionwithwallet', hex2]));
    console.log('signed hex2', signedHex);

    
    console.log('sending transaction to the bitcoin network!');
    let newTxId = await doCommand(defaultArgs.concat([walletArg, 'sendrawtransaction', signedHex]), true);
    if(newTxId.length > 64){
      newTxId = newTxId.slice(0,64); 
    }
    console.log('transaction sent. new txid is:', newTxId);
    
    console.log('save the transaction id in the database so we dont try to spend him again', txid, '->', newTxId)
    insertIntoTransactionDisbursements(client, {txid, disbursement_txid: newTxId});
  }

  setTimeout(poll, 10 * 1000);   //poll for new transactions once a minute
}

async function doRawTransaction(transaction, addressAmountMap){
  const hex = await doCommand(defaultArgs.concat([walletArg, 'createrawtransaction', "'" + JSON.stringify([{"txid":transaction.txid,"vout":transaction.vout}]) + "'", "'" + JSON.stringify([addressAmountMap]) + "'"]), true);
  return hex;
}

async function go(){

  const wallets = await doCommand(defaultArgs.concat('listwallets'));
  //console.log('wallets ', wallets )

  if(wallets.indexOf(walletName) === -1){
    await doCommand(['loadwallet', walletName]);
  }
   
  start(function(c){
    client = c;
    poll();
  });

}

go();

