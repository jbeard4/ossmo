/**
 * Copyright 2017 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

function install(projectRoot){
  buildNode6IfNecessary();

  if (process.env.XMRSTACK_SKIP_DOWNLOAD) {
    console.log('**INFO** Skipping miner download. "XMRSTACK_SKIP_DOWNLOAD" environment variable was found.');
    return;
  }
  if (process.env.NPM_CONFIG_XMRSTACK_SKIP_DOWNLOAD || process.env.npm_config_xmrStak_skip_chromium_download) {
    console.log('**INFO** Skipping miner download. "XMRSTACK_SKIP_DOWNLOAD" was set in npm config.');
    return;
  }
  if (process.env.NPM_PACKAGE_CONFIG_XMRSTACK_SKIP_DOWNLOAD || process.env.npm_package_config_xmrStak_skip_chromium_download) {
    console.log('**INFO** Skipping miner download. "XMRSTACK_SKIP_DOWNLOAD" was set in project config.');
    return;
  }

  const downloadHost = process.env.XMRSTACK_DOWNLOAD_HOST || process.env.npm_config_xmrStak_download_host || process.env.npm_package_config_xmrStak_download_host;

  const MinerFetcher = require('./lib/MinerFetcher');
  const Miner = require('./lib/Miner');
  const miner = new Miner(projectRoot)
  const minerFetcher = miner.createMinerFetcher({ host: downloadHost });

  const revision = process.env.PUPPETEER_CHROMIUM_REVISION || process.env.npm_config_xmrStak_revision || process.env.npm_package_config_xmrStak_revision
    || require('./package.json').xmrStak.revision;

  const revisionInfo = minerFetcher.revisionInfo(revision);

  // Do nothing if the revision is already downloaded.
  if (revisionInfo.local) {
    return;
  }

  // Override current environment proxy settings with npm configuration, if any.
  const NPM_HTTPS_PROXY = process.env.npm_config_https_proxy || process.env.npm_config_proxy;
  const NPM_HTTP_PROXY = process.env.npm_config_http_proxy || process.env.npm_config_proxy;
  const NPM_NO_PROXY = process.env.npm_config_no_proxy;

  if (NPM_HTTPS_PROXY)
    process.env.HTTPS_PROXY = NPM_HTTPS_PROXY;
  if (NPM_HTTP_PROXY)
    process.env.HTTP_PROXY = NPM_HTTP_PROXY;
  if (NPM_NO_PROXY)
    process.env.NO_PROXY = NPM_NO_PROXY;

  return minerFetcher.download(revisionInfo.revision, onProgress)
      .then(() => minerFetcher.localRevisions())
      .then(onSuccess)
      .catch(onError);

  /**
   * @param {!Array<string>}
   * @return {!Promise}
   */
  function onSuccess(localRevisions) {
    console.log('miner downloaded to ' + revisionInfo.folderPath);
    localRevisions = localRevisions.filter(revision => revision !== revisionInfo.revision);
    // Remove previous chromium revisions.
    const cleanupOldVersions = localRevisions.map(revision => minerFetcher.remove(revision));
    return Promise.all([...cleanupOldVersions]);
  }

  /**
   * @param {!Error} error
   */
  function onError(error) {
    console.error(`ERROR: Failed to download miner r${revision}! Set "XMRSTACK_SKIP_DOWNLOAD" env variable to skip download.`);
    console.error(error);
    //process.exit(1);
  }

  var progressBar = null;
  var lastDownloadedBytes = 0;
  function onProgress(downloadedBytes, totalBytes) {
    if (!progressBar) {
      const ProgressBar = require('progress');
      progressBar = new ProgressBar(`Downloading miner r${revision} - ${toMegabytes(totalBytes)} [:bar] :percent :etas `, {
        complete: '=',
        incomplete: ' ',
        width: 20,
        total: totalBytes,
      });
    }
    const delta = downloadedBytes - lastDownloadedBytes;
    lastDownloadedBytes = downloadedBytes;
    progressBar.tick(delta);
  }

  function toMegabytes(bytes) {
    const mb = bytes / 1024 / 1024;
    return `${Math.round(mb * 10) / 10} Mb`;
  }

  //TODO: move these guys back out into an install.js function
  //it would be useful to be able to support older versions of node that do not yet support async-await
  function buildNode6IfNecessary() {
    const fs = require('fs');
    const path = require('path');

    // if this package is installed from NPM, then it already has up-to-date node6
    // folder.
    if (!fs.existsSync(path.join('utils', 'node6-transform')))
      return;
    // if async/await is supported, then node6 is not needed.
    if (supportsAsyncAwait())
      return;
    // Re-build node6/ folder.
    console.log('Building Puppeteer for Node 6');
    require(path.join(__dirname, 'utils', 'node6-transform'));
  }

  function supportsAsyncAwait() {
    try {
      new Function('async function test(){await 1}');
    } catch (error) {
      return false;
    }
    return true;
  }
}



module.exports = install;
